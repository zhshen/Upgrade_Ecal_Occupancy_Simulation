void pidchange()
{
//=========Macro generated from canvas: c/PID
//=========  (Thu Aug  9 13:49:58 2018) by ROOT version 6.14/02
   TCanvas *c = new TCanvas("c", "PID",520,241,1018,600);
   gStyle->SetOptStat(0);
   c->Range(-0.8750001,-3.110771,7.875,0.5021031);
   c->SetFillColor(0);
   c->SetBorderMode(0);
   c->SetBorderSize(2);
   c->SetLogy();
   c->SetFrameBorderMode(0);
   c->SetFrameBorderMode(0);
   
   TH1F *pidplot__1 = new TH1F("pidplot__1","pid:fraction_vs_type",7,0,7);
   pidplot__1->SetBinContent(5,0.08296436);
   pidplot__1->SetBinContent(1,0.7283769);
   pidplot__1->SetBinContent(4,0.003669372);
   pidplot__1->SetBinContent(6,0.005931368);
   pidplot__1->SetBinContent(3,0.0500094);
   pidplot__1->SetBinContent(2,0.1169164);
   pidplot__1->SetBinContent(7,0.01314469);
   pidplot__1->SetBinError(5,0.0005162951);
   pidplot__1->SetBinError(1,0.001528761);
   pidplot__1->SetBinError(4,0.0001085829);
   pidplot__1->SetBinError(6,0.0001380525);
   pidplot__1->SetBinError(3,0.0004008179);
   pidplot__1->SetBinError(2,0.0006129588);
   pidplot__1->SetBinError(7,0.0002055148);
   pidplot__1->SetEntries(311223);
   pidplot__1->SetStats(0);
   pidplot__1->SetFillColor(38);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   pidplot__1->SetLineColor(ci);
   pidplot__1->GetXaxis()->SetTitle("type");
   
   pidplot__1->GetXaxis()->SetBinLabel(1,"#gamma");
   pidplot__1->GetXaxis()->SetBinLabel(2,"e^{+}e^{-}");
   pidplot__1->GetXaxis()->SetBinLabel(3,"#pi^{+}#pi^{-}");
   pidplot__1->GetXaxis()->SetBinLabel(4,"K^{+}K^{-}");
   pidplot__1->GetXaxis()->SetBinLabel(5,"nn~");
   pidplot__1->GetXaxis()->SetBinLabel(6,"p^{+}p^{-}");
   pidplot__1->GetXaxis()->SetBinLabel(7,"others");
   
   pidplot__1->GetXaxis()->CenterTitle(true);
   pidplot__1->GetXaxis()->SetLabelFont(42);
   pidplot__1->GetXaxis()->SetLabelSize(0.035);
   pidplot__1->GetXaxis()->SetTitleSize(0.045);
   pidplot__1->GetXaxis()->SetTitleFont(42);
   pidplot__1->GetYaxis()->SetTitle("fraction");
   pidplot__1->GetYaxis()->SetLabelFont(42);
   pidplot__1->GetYaxis()->SetLabelSize(0.035);
   pidplot__1->GetYaxis()->SetTitleSize(0.045);
   pidplot__1->GetYaxis()->SetTitleOffset(0.9);
   pidplot__1->GetYaxis()->SetTitleFont(42);
   pidplot__1->GetZaxis()->SetLabelFont(42);
   pidplot__1->GetZaxis()->SetLabelSize(0.035);
   pidplot__1->GetZaxis()->SetTitleSize(0.035);
   pidplot__1->GetZaxis()->SetTitleFont(42);
   pidplot__1->Draw("bar");
   
   TPaveText *pt = new TPaveText(0.3507283,0.9343728,0.6492717,0.995,"blNDC");
   pt->SetName("title");
   pt->SetBorderSize(0);
   pt->SetFillColor(0);
   pt->SetFillStyle(0);
   pt->SetTextFont(42);
   TText *pt_LaTex = pt->AddText("pid:fraction_vs_type");
   pt->Draw();
   c->Modified();
   c->cd();
   c->SetSelected(c);
   c->Print("../plot_nu7.6/pid.gif");
}
