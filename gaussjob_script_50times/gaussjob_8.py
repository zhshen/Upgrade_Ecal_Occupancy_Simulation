from Gaudi.Configuration import *

##importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6-HorExtAngle.py")
importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu11.4-HorExtAngle.py")
importOptions("$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py")
importOptions("$DECFILESROOT/options/30000000.py")
importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
importOptions("$APPCONFIGOPTS/Gauss/Gauss-Upgrade-Baseline-20150522.py")
importOptions("$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py")
#importOptions("$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py")
importOptions("$APPCONFIGOPTS/Gauss/Upgrade.py")

### redefine lumi
from Configurables import Gauss 
import GaudiKernel.SystemOfUnits as SoU
#lumi     = 1.e+34 / ( SoU.cm2 * SoU.s ) 
lumi     = 2.e+34 / ( SoU.cm2 * SoU.s ) 
nbanches = 2400 
Gauss().Luminosity = lumi / nbanches 
###
from Gauss.Configuration import GenInit

GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber = 1128

from Configurables import CondDB, Gauss 
CondDB().Upgrade = True
CondDB().Tags['DDDB'] = 'ecal-occupancy-simulation'


from Configurables import LHCbApp

LHCbApp().DDDBtag = 'upgrade/ecal-occupancy-simulation' 
LHCbApp().CondDBtag ='upgrade/sim-20171127-vc-md100'  
LHCbApp().Simulation = True

LHCbApp().EvtMax = 50


##Gauss().DetectorGeo  = {'Detectors': ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet']}
Gauss().DetectorGeo  = {'Detectors': ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet']}
Gauss.DetectorSim = {"Detectors": [ 'Magnet'] }
Gauss.DetectorMoni ={"Detectors": [ 'Magnet'] }


## speed-up it
Gauss().SpilloverPaths = [] 
Gauss().Phases = [ 'Generator' , 'Simulation' ] 
Gauss().OutputType = 'NONE' 

from Configurables import GiGaGeo, GaussSensPlaneDet, TupleTool

gg = GiGaGeo()
gg.addTool ( GaussSensPlaneDet , name = 'PlaneDet' )
pd = gg.PlaneDet 
pd.OutputLevel = 1 
pd.addTool ( TupleTool )

from Configurables import ApplicationMgr, NTupleSvc 
NTupleSvc().Output = [ 
    "FILE1 DATAFILE='./root_file_50times/mytuple_50times_08.root' TYP='ROOT' OPT='NEW'"
]
ApplicationMgr().ExtSvc += [ NTupleSvc() ] 

print Gauss() 
