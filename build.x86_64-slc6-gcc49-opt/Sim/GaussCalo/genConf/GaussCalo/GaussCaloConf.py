#Sat Jul 28 23:16:02 2018"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.Proxy.Configurable import *

class EcalSensDet( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'GiGaService' : 'GiGa', # str
    'GiGaSetUpService' : 'GiGa', # str
    'Active' : True, # bool
    'DetectorPath' : '', # str
    'StartVolumes' : [  ], # list
    'EndVolume' : 'World', # str
    'CollectionName' : 'Hits', # str
    'Detector' : '/dd/Structure/LHCb/DownstreamRegion/Ecal', # str
    'zMin' : -1000000.0, # float
    'zMax' : 1000000.0, # float
    'BirkC1' : 8.1139625e+17, # float
    'BirkC1cor' : 0.57142857, # float
    'BirkC2' : 3.7398185e+34, # float
    'dT0' : 0.50000000, # float
    'Histograms' : [  ], # list
    'SlotWidth' : 25.000000, # float
    'a_local_inner_ecal' : 0.0000000, # float
    'a_local_middle_ecal' : 0.0000000, # float
    'a_local_outer_ecal' : 0.0000000, # float
    'a_global_inner_ecal' : 0.00040000000, # float
    'a_global_middle_ecal' : 0.0020000000, # float
    'a_global_outer_ecal' : 0.030000000, # float
    'a_reflection_height' : 0.090000000, # float
    'a_reflection_width' : 6.0000000, # float
  }
  _propertyDocDct = { 
    'a_reflection_width' : """ none [unknown owner type] """,
    'a_reflection_height' : """ none [unknown owner type] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'DetectorPath' : """ none [unknown owner type] """,
    'BirkC2' : """ none [unknown owner type] """,
    'a_local_middle_ecal' : """ none [unknown owner type] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'GiGaService' : """ none [unknown owner type] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'ExtraOutputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'ExtraInputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'a_global_outer_ecal' : """ none [unknown owner type] """,
    'zMin' : """ none [unknown owner type] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'CollectionName' : """ none [unknown owner type] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'a_local_outer_ecal' : """ none [unknown owner type] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'zMax' : """ none [unknown owner type] """,
    'dT0' : """ none [unknown owner type] """,
    'a_local_inner_ecal' : """ none [unknown owner type] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'GiGaSetUpService' : """ none [unknown owner type] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'Active' : """ none [unknown owner type] """,
    'a_global_inner_ecal' : """ none [unknown owner type] """,
    'Histograms' : """ none [unknown owner type] """,
    'Detector' : """ none [unknown owner type] """,
    'BirkC1' : """ none [unknown owner type] """,
    'EndVolume' : """ none [unknown owner type] """,
    'BirkC1cor' : """ none [unknown owner type] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'StartVolumes' : """ none [unknown owner type] """,
    'SlotWidth' : """ none [unknown owner type] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'a_global_middle_ecal' : """ none [unknown owner type] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(EcalSensDet, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'GaussCalo'
  def getType( self ):
      return 'EcalSensDet'
  pass # class EcalSensDet

class GaussSensPlaneDet( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'GiGaService' : 'GiGa', # str
    'GiGaSetUpService' : 'GiGa', # str
    'Active' : True, # bool
    'DetectorPath' : '', # str
    'CollectionName' : 'Hits', # str
    'KeepAllLinks' : False, # bool
    'OneEntry' : True, # bool
    'CutForPhoton' : 50.000000, # float
    'CutForElectron' : 10.000000, # float
    'CutForPositron' : 10.000000, # float
    'CutForMuon' : -1.0000000, # float
    'CutForCharged' : 10.000000, # float
    'CutForNeutral' : 10.000000, # float
  }
  _propertyDocDct = { 
    'CutForNeutral' : """ none [unknown owner type] """,
    'CutForMuon' : """ none [unknown owner type] """,
    'CutForPositron' : """ none [unknown owner type] """,
    'CutForPhoton' : """ none [unknown owner type] """,
    'KeepAllLinks' : """ none [unknown owner type] """,
    'Active' : """ none [unknown owner type] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'OneEntry' : """ none [unknown owner type] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'CollectionName' : """ none [unknown owner type] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'CutForElectron' : """ none [unknown owner type] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'ExtraInputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'CutForCharged' : """ none [unknown owner type] """,
    'ExtraOutputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'GiGaService' : """ none [unknown owner type] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'GiGaSetUpService' : """ none [unknown owner type] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'DetectorPath' : """ none [unknown owner type] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(GaussSensPlaneDet, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'GaussCalo'
  def getType( self ):
      return 'GaussSensPlaneDet'
  pass # class GaussSensPlaneDet

class GetCaloHitsAlg( ConfigurableAlgorithm ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'Enable' : True, # bool
    'ErrorMax' : 1, # int
    'AuditAlgorithms' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'AuditExecute' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditBeginRun' : False, # bool
    'AuditEndRun' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'Timeline' : True, # bool
    'MonitorService' : 'MonitorSvc', # str
    'RegisterForContextService' : True, # bool
    'IsClonable' : False, # bool
    'Cardinality' : 1, # int
    'NeededResources' : [  ], # list
    'IsIOBound' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'VetoObjects' : [  ], # list
    'RequireObjects' : [  ], # list
    'GiGaService' : 'GiGa', # str
    'KineCnvService' : 'GiGaKine', # str
    'MCHitsLocation' : '', # str
    'CollectionName' : '', # str
    'MCParticles' : 'MC/Particles', # str
  }
  _propertyDocDct = { 
    'MCParticles' : """ none [unknown owner type] """,
    'RequireObjects' : """ execute only if one or more of these TES objects exist [GaudiAlgorithm] """,
    'VetoObjects' : """ skip execute if one or more of these TES objects exist [GaudiAlgorithm] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'GiGaService' : """ none [unknown owner type] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<Algorithm>] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<Algorithm>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<Algorithm>] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<Algorithm>] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<Algorithm>] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<Algorithm>] """,
    'Timeline' : """ send events to TimelineSvc [Algorithm] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<Algorithm>] """,
    'ExtraInputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'CollectionName' : """ none [unknown owner type] """,
    'MCHitsLocation' : """ none [unknown owner type] """,
    'AuditAlgorithms' : """ [[deprecated]] unused [Algorithm] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<Algorithm>] """,
    'ExtraOutputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgorithm,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<Algorithm>] """,
    'ErrorMax' : """ [[deprecated]] max number of errors [Algorithm] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [Algorithm] """,
    'AuditInitialize' : """ trigger auditor on initialize() [Algorithm] """,
    'OutputLevel' : """ output level [Algorithm] """,
    'AuditStart' : """ trigger auditor on start() [Algorithm] """,
    'AuditFinalize' : """ trigger auditor on finalize() [Algorithm] """,
    'AuditBeginRun' : """ trigger auditor on beginRun() [Algorithm] """,
    'AuditStop' : """ trigger auditor on stop() [Algorithm] """,
    'AuditExecute' : """ trigger auditor on execute() [Algorithm] """,
    'AuditEndRun' : """ trigger auditor on endRun() [Algorithm] """,
    'AuditRestart' : """ trigger auditor on restart() [Algorithm] """,
    'IsClonable' : """ thread-safe enough for cloning? [Algorithm] """,
    'KineCnvService' : """ none [unknown owner type] """,
    'NeededResources' : """ named resources needed during event looping [Algorithm] """,
    'MonitorService' : """ name to use for Monitor Service [Algorithm] """,
    'RegisterForContextService' : """ flag to enforce the registration for Algorithm Context Service [Algorithm] """,
    'Enable' : """ should the algorithm be executed or not [Algorithm] """,
    'IsIOBound' : """ if the algorithm is I/O-bound (in the broad sense of Von Neumann bottleneck) [Algorithm] """,
    'Cardinality' : """ how many clones to create - 0 means algo is reentrant [Algorithm] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(GetCaloHitsAlg, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'GaussCalo'
  def getType( self ):
      return 'GetCaloHitsAlg'
  pass # class GetCaloHitsAlg

class HcalSensDet( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'GiGaService' : 'GiGa', # str
    'GiGaSetUpService' : 'GiGa', # str
    'Active' : True, # bool
    'DetectorPath' : '', # str
    'StartVolumes' : [  ], # list
    'EndVolume' : 'World', # str
    'CollectionName' : 'Hits', # str
    'Detector' : '/dd/Structure/LHCb/DownstreamRegion/Ecal', # str
    'zMin' : -1000000.0, # float
    'zMax' : 1000000.0, # float
    'BirkC1' : 8.1139625e+17, # float
    'BirkC1cor' : 0.57142857, # float
    'BirkC2' : 3.7398185e+34, # float
    'dT0' : 0.50000000, # float
    'Histograms' : [  ], # list
    'SlotWidth' : 25.000000, # float
  }
  _propertyDocDct = { 
    'SlotWidth' : """ none [unknown owner type] """,
    'BirkC1cor' : """ none [unknown owner type] """,
    'BirkC1' : """ none [unknown owner type] """,
    'Detector' : """ none [unknown owner type] """,
    'EndVolume' : """ none [unknown owner type] """,
    'StartVolumes' : """ none [unknown owner type] """,
    'Active' : """ none [unknown owner type] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'dT0' : """ none [unknown owner type] """,
    'zMax' : """ none [unknown owner type] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'CollectionName' : """ none [unknown owner type] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'zMin' : """ none [unknown owner type] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'ExtraInputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'Histograms' : """ none [unknown owner type] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'ExtraOutputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'GiGaService' : """ none [unknown owner type] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'BirkC2' : """ none [unknown owner type] """,
    'GiGaSetUpService' : """ none [unknown owner type] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'DetectorPath' : """ none [unknown owner type] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(HcalSensDet, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'GaussCalo'
  def getType( self ):
      return 'HcalSensDet'
  pass # class HcalSensDet

class SpdPrsSensDet( ConfigurableAlgTool ) :
  __slots__ = { 
    'ExtraInputs' : [], # list
    'ExtraOutputs' : [], # list
    'OutputLevel' : 0, # int
    'MonitorService' : 'MonitorSvc', # str
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'AuditReinitialize' : False, # bool
    'AuditRestart' : False, # bool
    'ErrorsPrint' : True, # bool
    'PropertiesPrint' : False, # bool
    'StatPrint' : True, # bool
    'TypePrint' : True, # bool
    'Context' : '', # str
    'RootInTES' : '', # str
    'StatTableHeader' : ' |    Counter                                      |     #     |    sum     | mean/eff^* | rms/err^*  |     min     |     max     |', # str
    'RegularRowFormat' : ' | %|-48.48s|%|50t||%|10d| |%|11.7g| |%|#11.5g| |%|#11.5g| |%|#12.5g| |%|#12.5g| |', # str
    'EfficiencyRowFormat' : ' |*%|-48.48s|%|50t||%|10d| |%|11.5g| |(%|#9.6g| +- %|-#9.6g|)%%|   -------   |   -------   |', # str
    'UseEfficiencyRowFormat' : True, # bool
    'CounterList' : [ '.*' ], # list
    'StatEntityList' : [  ], # list
    'ContextService' : 'AlgContextSvc', # str
    'GiGaService' : 'GiGa', # str
    'GiGaSetUpService' : 'GiGa', # str
    'Active' : True, # bool
    'DetectorPath' : '', # str
    'StartVolumes' : [  ], # list
    'EndVolume' : 'World', # str
    'CollectionName' : 'Hits', # str
    'Detector' : '/dd/Structure/LHCb/DownstreamRegion/Ecal', # str
    'zMin' : -1000000.0, # float
    'zMax' : 1000000.0, # float
    'BirkC1' : 8.1139625e+17, # float
    'BirkC1cor' : 0.57142857, # float
    'BirkC2' : 3.7398185e+34, # float
    'dT0' : 0.50000000, # float
    'Histograms' : [  ], # list
    'BunchCrossing' : 25.000000, # float
    'NumberBXs' : 6, # int
    'IntegrationDelays' : [ 0.0000000 , 0.0000000 , 0.0000000 ], # list
    'FracMin' : 1.0000000e-05, # float
  }
  _propertyDocDct = { 
    'FracMin' : """ none [unknown owner type] """,
    'BirkC1cor' : """ none [unknown owner type] """,
    'BirkC1' : """ none [unknown owner type] """,
    'Detector' : """ none [unknown owner type] """,
    'EndVolume' : """ none [unknown owner type] """,
    'StartVolumes' : """ none [unknown owner type] """,
    'Active' : """ none [unknown owner type] """,
    'ContextService' : """ the name of Algorithm Context Service [GaudiTool] """,
    'StatEntityList' : """ RegEx list, of StatEntity counters for CounterSummary [GaudiCommon<AlgTool>] """,
    'AuditFinalize' : """ trigger auditor on finalize() [AlgTool] """,
    'AuditStop' : """ trigger auditor on stop() [AlgTool] """,
    'BunchCrossing' : """ none [unknown owner type] """,
    'EfficiencyRowFormat' : """ The format for "efficiency" row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'dT0' : """ none [unknown owner type] """,
    'zMax' : """ none [unknown owner type] """,
    'AuditStart' : """ trigger auditor on start() [AlgTool] """,
    'NumberBXs' : """ none [unknown owner type] """,
    'OutputLevel' : """ output level [AlgTool] """,
    'IntegrationDelays' : """ none [unknown owner type] """,
    'AuditReinitialize' : """ trigger auditor on reinitialize() [AlgTool] """,
    'AuditInitialize' : """ trigger auditor on initialize() [AlgTool] """,
    'CollectionName' : """ none [unknown owner type] """,
    'AuditTools' : """ [[deprecated]] unused [AlgTool] """,
    'RegularRowFormat' : """ the format for regular row in the output Stat-table [GaudiCommon<AlgTool>] """,
    'MonitorService' : """ name to use for Monitor Service [AlgTool] """,
    'AuditRestart' : """ trigger auditor on restart() [AlgTool] """,
    'zMin' : """ none [unknown owner type] """,
    'ErrorsPrint' : """ print the statistics of errors/warnings/exceptions [GaudiCommon<AlgTool>] """,
    'ExtraInputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'PropertiesPrint' : """ print the properties of the component [GaudiCommon<AlgTool>] """,
    'Histograms' : """ none [unknown owner type] """,
    'TypePrint' : """ add the actual C++ component type into the messages [GaudiCommon<AlgTool>] """,
    'ExtraOutputs' : """ [[deprecated]] [DataHandleHolderBase<PropertyHolder<CommonMessaging<implements<IAlgTool,IDataHandleHolder,IProperty,IStateful> > > >] """,
    'StatPrint' : """ print the table of counters [GaudiCommon<AlgTool>] """,
    'Context' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'GiGaService' : """ none [unknown owner type] """,
    'UseEfficiencyRowFormat' : """ use the special format for printout of efficiency counters [GaudiCommon<AlgTool>] """,
    'BirkC2' : """ none [unknown owner type] """,
    'GiGaSetUpService' : """ none [unknown owner type] """,
    'StatTableHeader' : """ the header row for the output Stat-table [GaudiCommon<AlgTool>] """,
    'DetectorPath' : """ none [unknown owner type] """,
    'RootInTES' : """ note: overridden by parent settings [GaudiCommon<AlgTool>] """,
    'CounterList' : """ RegEx list, of simple integer counters for CounterSummary [GaudiCommon<AlgTool>] """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(SpdPrsSensDet, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'GaussCalo'
  def getType( self ):
      return 'SpdPrsSensDet'
  pass # class SpdPrsSensDet
