# Install script for directory: /afs/cern.ch/work/z/zhshen/GaussDev_v51r2

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/InstallArea/x86_64-slc6-gcc49-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/cmake" TYPE DIRECTORY FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/cmake/" FILES_MATCHING REGEX "/[^/]*\\.cmake$" REGEX "/[^/]*\\.cmake\\.in$" REGEX "/[^/]*\\.py$" REGEX "/\\.svn$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/compile_commands.json")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/include/GAUSSDEV_VERSION.h")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(NOT EXISTS /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.confdb)
                  message(WARNING "creating partial /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.confdb")
                  execute_process(COMMAND /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_88/Python/2.7.13/x86_64-slc6-gcc49-opt/bin/python2.7;/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p4/LbUtils/cmake/quick-merge --ignore-missing /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/Sim/GaussCalo/genConf/GaussCalo/GaussCalo.confdb /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.confdb)
                  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.confdb")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  if(NOT EXISTS /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.components)
                  message(WARNING "creating partial /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.components")
                  execute_process(COMMAND /cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_88/Python/2.7.13/x86_64-slc6-gcc49-opt/bin/python2.7;/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p4/LbUtils/cmake/quick-merge --ignore-missing /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/Sim/GaussCalo/GaussCalo.components /afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.components)
                  endif()
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE FILE OPTIONAL FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/lib/GaussDev.components")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/config/GaussDev.xenv")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/config/GaussDevConfigVersion.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/config/GaussDevConfig.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/cmake" TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/config/GaussDevPlatformConfig.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/cmake" TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/config/GaussCaloExport.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/." TYPE FILE FILES "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/config/manifest.xml")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/Sim/GaussCalo/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/afs/cern.ch/work/z/zhshen/GaussDev_v51r2/build.x86_64-slc6-gcc49-opt/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
