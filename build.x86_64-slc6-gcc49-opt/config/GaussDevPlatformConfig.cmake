# File automatically generated: DO NOT EDIT.

# Get the exported informations about the targets
get_filename_component(_dir "${CMAKE_CURRENT_LIST_FILE}" PATH)

# Set useful properties
get_filename_component(_dir "${_dir}" PATH)
set(GaussDev_INCLUDE_DIRS ${_dir}/include)
set(GaussDev_LIBRARY_DIRS ${_dir}/lib)

set(GaussDev_BINARY_PATH ${_dir}/bin ${_dir}/scripts)
set(GaussDev_PYTHON_PATH ${_dir}/python)

set(GaussDev_COMPONENT_LIBRARIES GaussCalo)
set(GaussDev_LINKER_LIBRARIES )

set(GaussDev_ENVIRONMENT PREPEND;PYTHONPATH;\${LCG_releases_base}/LCG_88/ROOT/6.08.06/x86_64-slc6-gcc49-opt/lib;PREPEND;PATH;\${LCG_releases_base}/gcc/4.9.3/x86_64-slc6/bin;PREPEND;PATH;\${LCG_releases_base}/LCG_88/Python/2.7.13/x86_64-slc6-gcc49-opt/bin;PREPEND;PATH;\${LCG_releases_base}/LCG_88/ROOT/6.08.06/x86_64-slc6-gcc49-opt/bin;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/ROOT/6.08.06/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/Boost/1.62.0/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/tbb/44_20160413/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/GSL/2.1/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/clhep/2.3.1.1/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/vdt/0.3.6/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_88/HepMC/2.06.09/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/GEANT4/GEANT4_v96r4p7/InstallArea/x86_64-slc6-gcc49-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/gcc/4.9.3/x86_64-slc6/lib64;SET;ROOTSYS;\${LCG_releases_base}/LCG_88/ROOT/6.08.06/x86_64-slc6-gcc49-opt;SET;PYTHONHOME;\${LCG_releases_base}/LCG_88/Python/2.7.13/x86_64-slc6-gcc49-opt;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v28r2/InstallArea/x86_64-slc6-gcc49-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/GEANT4/GEANT4_v96r4p7/InstallArea/x86_64-slc6-gcc49-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v42r8/InstallArea/x86_64-slc6-gcc49-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/GAUSS/GAUSS_v51r2/InstallArea/x86_64-slc6-gcc49-opt/include;PREPEND;ROOT_INCLUDE_PATH;\${LCG_releases_base}/LCG_88/AIDA/3.2.1/x86_64-slc6-gcc49-opt/src/cpp;SET;GAUDIAPPNAME;GaussDev;SET;GAUDIAPPVERSION;v51r2;PREPEND;PATH;\${.}/scripts;PREPEND;PATH;\${.}/bin;PREPEND;LD_LIBRARY_PATH;\${.}/lib;PREPEND;ROOT_INCLUDE_PATH;\${.}/include;PREPEND;PYTHONPATH;\${.}/python;PREPEND;PYTHONPATH;\${.}/python/lib-dynload;SET;GAUSSDEV_PROJECT_ROOT;\${.}/../../;SET;GAUSSCALOROOT;\${GAUSSDEV_PROJECT_ROOT}/Sim/GaussCalo;SET;GAUSSCALOOPTS;\${GAUSSCALOROOT}/options)

set(GaussDev_EXPORTED_SUBDIRS)
foreach(p Sim/GaussCalo)
  get_filename_component(pn ${p} NAME)
  if(EXISTS ${_dir}/cmake/${pn}Export.cmake)
    set(GaussDev_EXPORTED_SUBDIRS ${GaussDev_EXPORTED_SUBDIRS} ${p})
  endif()
endforeach()

set(GaussDev_OVERRIDDEN_SUBDIRS )
