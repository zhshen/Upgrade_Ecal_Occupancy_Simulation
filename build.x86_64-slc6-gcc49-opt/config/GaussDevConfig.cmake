# File automatically generated: DO NOT EDIT.
set(GaussDev_heptools_version 88)
set(GaussDev_heptools_system x86_64-slc6-gcc49)

set(GaussDev_PLATFORM x86_64-slc6-gcc49-opt)

set(GaussDev_VERSION v51r2)
set(GaussDev_VERSION_MAJOR 51)
set(GaussDev_VERSION_MINOR 2)
set(GaussDev_VERSION_PATCH )

set(GaussDev_USES Gauss;v51r2)
set(GaussDev_DATA )

list(INSERT CMAKE_MODULE_PATH 0 ${GaussDev_DIR}/cmake)
include(GaussDevPlatformConfig)
