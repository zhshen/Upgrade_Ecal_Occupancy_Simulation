void etr2(){
   TChain *chain = new TChain("GiGaGeo.PlaneDet.Tuple/Hits");
   chain->Add("../mytuple_nu11.4_01.root");
   chain->Add("../mytuple_nu11.4_02.root");
   chain->Add("../mytuple_nu11.4_03.root");
   chain->Add("../mytuple_nu11.4_04.root");
   chain->Add("../mytuple_nu11.4_05.root");
   chain->Add("../mytuple_nu11.4_06.root");
   chain->Add("../mytuple_nu11.4_07.root");
   chain->Add("../mytuple_nu11.4_08.root");
   chain->Add("../mytuple_nu11.4_09.root");

   // Declaration of leaf types
   Int_t           n;
   Float_t         pid[20000];   //[n]
   Float_t         x[20000];   //[n]
   Float_t         y[20000];   //[n]
   Float_t         z[20000];   //[n]
   Float_t         t[20000];   //[n]
   Float_t         px[20000];   //[n]
   Float_t         py[20000];   //[n]
   Float_t         pz[20000];   //[n]
   Float_t         e[20000];   //[n]

   // List of branches
   TBranch        *b_n;   //!
   TBranch        *b_pid;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_t;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_e;   //!

   chain->SetBranchAddress("n", &n, &b_n);
   chain->SetBranchAddress("pid", pid, &b_pid);
   chain->SetBranchAddress("x", x, &b_x);
   chain->SetBranchAddress("y", y, &b_y);
   chain->SetBranchAddress("z", z, &b_z);
   chain->SetBranchAddress("t", t, &b_t);
   chain->SetBranchAddress("px", px, &b_px);
   chain->SetBranchAddress("py", py, &b_py);
   chain->SetBranchAddress("pz", pz, &b_pz);
   chain->SetBranchAddress("e", e, &b_e);


   gStyle->SetOptStat(0);

   TCanvas *c1 = new TCanvas("c1","nu11.4_et_deposition_density_vs_r(nu11.4)",0,0,1200,900        );//,520,206,820,600);

   int    a=8;
   double xmin=20;
   double xmax=+100;

   TH1F * xyplot1 = new TH1F("xyplot1","Tranverse Energy deposition density v.s. r"      ,a,xmin,xmax);
   TH1F * xyplot2 = new TH1F("xyplot2","nu11.4_e^{+}e^{-}:et_deposition_density_vs_r"    ,a,xmin,xmax);
   TH1F * xyplot3 = new TH1F("xyplot3","nu11.4_#pi^{+}#pi^{-}:et_deposition_density_vs_r",a,xmin,xmax);
   TH1F * xyplot4 = new TH1F("xyplot4","nu11.4_K^{+}K^{-}:et_deposition_density_vs_r"    ,a,xmin,xmax);
   TH1F * xyplot5 = new TH1F("xyplot5","nu11.4_nn~:et_deposition_density_vs_r"           ,a,xmin,xmax);
   TH1F * xyplot6 = new TH1F("xyplot6","nu11.4_p^{+}p^{-}:et_deposition_density_vs_r"    ,a,xmin,xmax);
   TH1F * xyplot7 = new TH1F("xyplot7","nu11.4_others:et_deposition_density_vs_r"        ,a,xmin,xmax);

   const double scale = 0.5 / TMath::Pi() / ( ( xmax - xmin ) / a );
   Long64_t nentries = chain->GetEntries();
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      chain->GetEntry(jentry);
      // if (Cut(ientry) < 0) continue;
		for (int nhits=0;nhits<n;nhits++) {
			if(pz[nhits]>0 && z[nhits]<12530. && z[nhits]>12505. && t[nhits]>40. && t[nhits]<50. ){
				double r=0;
				r=sqrt(x[nhits]*x[nhits]+y[nhits]*y[nhits])/10.0;
				const double z_cm = z[nhits] / 10 ;
				if(abs(pid[nhits])==22)        xyplot1->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
				else if(abs(pid[nhits])==11)   xyplot2->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
				else if(abs(pid[nhits])==211)  xyplot3->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
				else if(abs(pid[nhits])==321)  xyplot4->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
				else if(abs(pid[nhits])==2112) xyplot5->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
				else if(abs(pid[nhits])==2212) xyplot6->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
				else                           xyplot7->Fill(r,r/sqrt(r*r+z_cm*z_cm)*e[nhits]/nentries*scale/r);
			}
		}
   }

   double max=1e1;
   double min=1e-2;
   float size =1.5 ;
   c1->cd();
   xyplot1->GetYaxis()->SetTitle("E_{t} deposition density(MeV/cm^{2})" );
   xyplot1->GetXaxis()->SetTitle("r(cm)" );
   xyplot1->GetYaxis()->SetTitleSize(0.045);
   xyplot1->GetYaxis()->SetTitleOffset(1);
   xyplot1->GetXaxis()->SetTitleSize(0.045);
   xyplot1->GetXaxis()->CenterTitle();
   xyplot1->SetMaximum(max);
   xyplot1->SetMinimum(min);
   xyplot1->SetMarkerColor(2);
   xyplot1->SetMarkerSize(size);
   xyplot1->SetLineColor(2);
   xyplot1->SetLineWidth(2);
   xyplot1->SetMarkerStyle(47);
   xyplot1->Draw("P C hist");

   xyplot2->SetMaximum(max);
   xyplot2->SetMinimum(min);
   xyplot2->SetLineColor(3);
   xyplot2->SetLineWidth(2);
   xyplot2->SetMarkerColor(3);
   xyplot2->SetMarkerSize(size);
   xyplot2->SetMarkerStyle(20);
   xyplot2->Draw("same P C hist");

   xyplot3->SetMaximum(max);
   xyplot3->SetMinimum(min);
   xyplot3->SetLineColor(4);
   xyplot3->SetLineWidth(2);
   xyplot3->SetMarkerColor(4);
   xyplot3->SetMarkerSize(size);
   xyplot3->SetMarkerStyle(22);
   xyplot3->Draw("same P C hist");

   xyplot4->SetMaximum(max);
   xyplot4->SetMinimum(min);
   xyplot4->SetLineColor(12);
   xyplot4->SetLineWidth(2);
   xyplot4->SetMarkerColor(12);
   xyplot4->SetMarkerSize(size);
   xyplot4->SetMarkerStyle(47);
   xyplot4->Draw("same P C hist");


   xyplot5->SetMaximum(max);
   xyplot5->SetMinimum(min);
   xyplot5->SetLineColor(6);
   xyplot5->SetLineWidth(2);
   xyplot5->SetMarkerColor(6);
   xyplot5->SetMarkerSize(size);
   xyplot5->SetMarkerStyle(34);
   xyplot5->Draw("same P C hist");

   xyplot6->SetMaximum(max);
   xyplot6->SetMinimum(min);
   xyplot6->SetLineColor(38);
   xyplot6->SetLineWidth(2);
   xyplot6->SetMarkerColor(38);
   xyplot6->SetMarkerSize(size);
   xyplot6->SetMarkerStyle(23);
   xyplot6->Draw("same P C hist");

   xyplot7->SetMaximum(max);
   xyplot7->SetMinimum(min);
   xyplot7->SetLineColor(46);
   xyplot7->SetLineWidth(2);
   xyplot7->SetLineWidth(2);
   xyplot7->SetMarkerColor(46);
   xyplot7->SetMarkerSize(size);
   xyplot7->SetMarkerStyle(45);
   xyplot7->Draw("same P C hist");

   TLegend* l=new TLegend(0.6,0.7,0.85,0.9);
   l->SetNColumns(2);
   l->AddEntry("xyplot1","#gamma","p");
   l->AddEntry("xyplot2","e^{+}e^{-}","p");
   l->AddEntry("xyplot3","#pi^{+}#pi^{-}","p");
   l->AddEntry("xyplot4","K^{+}K^{-}","p");
   l->AddEntry("xyplot5","nn~","p");
   l->AddEntry("xyplot6","p^{+}p^{-}","p");
   l->AddEntry("xyplot7","others","p");
   l->Draw();

   c1->SetLogy();
   c1->Print("../plot_nu11.4/nu11.4_et_deposition_density(20-100).gif");

}
