void pide(){
   TChain *chain = new TChain("GiGaGeo.PlaneDet.Tuple/Hits");
   chain->Add("../mytuple_nu11.4_01.root");
   chain->Add("../mytuple_nu11.4_02.root");
   chain->Add("../mytuple_nu11.4_03.root");
   chain->Add("../mytuple_nu11.4_04.root");
   chain->Add("../mytuple_nu11.4_05.root");
   chain->Add("../mytuple_nu11.4_06.root");
   chain->Add("../mytuple_nu11.4_07.root");
   chain->Add("../mytuple_nu11.4_08.root");
   chain->Add("../mytuple_nu11.4_09.root");
   
   // Declaration of leaf types
   Int_t           n;
   Float_t         pid[20000];   //[n]
   Float_t         x[20000];   //[n]
   Float_t         y[20000];   //[n]
   Float_t         z[20000];   //[n]
   Float_t         t[20000];   //[n]
   Float_t         px[20000];   //[n]
   Float_t         py[20000];   //[n]
   Float_t         pz[20000];   //[n]
   Float_t         e[20000];   //[n]

   // List of branches
   TBranch        *b_n;   //!
   TBranch        *b_pid;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_t;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_e;   //!

   chain->SetBranchAddress("n", &n, &b_n);
   chain->SetBranchAddress("pid", pid, &b_pid);
   chain->SetBranchAddress("x", x, &b_x);
   chain->SetBranchAddress("y", y, &b_y);
   chain->SetBranchAddress("z", z, &b_z);
   chain->SetBranchAddress("t", t, &b_t);
   chain->SetBranchAddress("px", px, &b_px);
   chain->SetBranchAddress("py", py, &b_py);
   chain->SetBranchAddress("pz", pz, &b_pz);
   chain->SetBranchAddress("e", e, &b_e);

	const Int_t nx = 7;
   const char *p[nx] = {"#gamma","e^{+}e^{-}","#pi^{+}#pi^{-}","K^{+}K^{-}","p^{+}p^{-}", "nn~","others"};

   TH1F * pidplot = new TH1F("pidplot","pide:energy_fraction_vs_type",7,0,7);
   TCanvas *c = new TCanvas("c", "PID",520,206,1018,600);
   pidplot->SetFillColor(38);
   gPad->SetLogy();
	gStyle->SetOptStat(0);

	pidplot->SetMaximum(1.);
	pidplot->SetMinimum(1e-2);

	double etot=0;
   Double_t ntot=0;
   Long64_t nentries = chain->GetEntries();
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      chain->GetEntry(jentry);
      // if (Cut(ientry) < 0) continue;
		for (int nhits=0;nhits<n;nhits++) {
			 if(pz[nhits]>0 && z[nhits]<12530. && z[nhits]>12505. && t[nhits]>40. && t[nhits]<50. )
			 {
				 etot=etot+e[nhits];
			 }
		 }
   }
   
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      chain->GetEntry(jentry);
      // if (Cut(ientry) < 0) continue;
		 for (int nhits=0;nhits<n;nhits++) {
			 if(pz[nhits]>0 && z[nhits]<12530. && z[nhits]>12505. && t[nhits]>40. && t[nhits]<50. )
			 {
				   if(pid[nhits]==22)               pidplot->Fill(p[0],e[nhits]/etot);
					else if(abs(pid[nhits])==11)     pidplot->Fill(p[1],e[nhits]/etot);
					else if(abs(pid[nhits])==211)    pidplot->Fill(p[2],e[nhits]/etot);
					else if(abs(pid[nhits])==321)    pidplot->Fill(p[3],e[nhits]/etot);
					else if(abs(pid[nhits])==2212)   pidplot->Fill(p[4],e[nhits]/etot);
					else if(abs(pid[nhits])==2112)   pidplot->Fill(p[5],e[nhits]/etot);
					else                             pidplot->Fill(p[6],e[nhits]/etot);
			 }
		 }
   }
   
   
   pidplot->GetXaxis()->SetTitle("type" );
   pidplot->GetYaxis()->SetTitle("fraction" );
   pidplot->GetYaxis()->SetTitleSize(0.045);
   pidplot->GetYaxis()->SetTitleOffset(0.9);
   pidplot->GetXaxis()->SetTitleSize(0.045);
   pidplot->GetXaxis()->CenterTitle();
   pidplot->Draw("bar");
	c->Modified();
}
