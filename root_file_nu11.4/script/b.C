void b(){
   TChain *chain = new TChain("GiGaGeo.PlaneDet.Tuple/Hits");
   chain->Add("../mytuple_nu11.4_01.root");
   chain->Add("../mytuple_nu11.4_02.root");
   chain->Add("../mytuple_nu11.4_03.root");
   chain->Add("../mytuple_nu11.4_04.root");
   chain->Add("../mytuple_nu11.4_05.root");
   chain->Add("../mytuple_nu11.4_06.root");
   chain->Add("../mytuple_nu11.4_07.root");
   chain->Add("../mytuple_nu11.4_08.root");
   chain->Add("../mytuple_nu11.4_09.root");

   // Declaration of leaf types
   Int_t           n;
   Float_t         pid[20000];   //[n]
   Float_t         x[20000];   //[n]
   Float_t         y[20000];   //[n]
   Float_t         z[20000];   //[n]
   Float_t         t[20000];   //[n]
   Float_t         px[20000];   //[n]
   Float_t         py[20000];   //[n]
   Float_t         pz[20000];   //[n]
   Float_t         e[20000];   //[n]

   // List of branches
   TBranch        *b_n;   //!
   TBranch        *b_pid;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_t;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_e;   //!

   chain->SetBranchAddress("n", &n, &b_n);
   chain->SetBranchAddress("pid", pid, &b_pid);
   chain->SetBranchAddress("x", x, &b_x);
   chain->SetBranchAddress("y", y, &b_y);
   chain->SetBranchAddress("z", z, &b_z);
   chain->SetBranchAddress("t", t, &b_t);
   chain->SetBranchAddress("px", px, &b_px);
   chain->SetBranchAddress("py", py, &b_py);
   chain->SetBranchAddress("pz", pz, &b_pz);
   chain->SetBranchAddress("e", e, &b_e);


   gStyle->SetOptStat(0);

   TCanvas *c1 = new TCanvas("c1","nu11.4_#gammaE:y_vs_x"        );//,520,206,820,600);
   TCanvas *c2 = new TCanvas("c2","nu11.4_e^{+}e^{-}E:y_vs_x"    );//,520,206,820,600);
   TCanvas *c3 = new TCanvas("c3","nu11.4_#pi^{+}#pi^{-}E:y_vs_x");//,520,206,820,600);
   TCanvas *c4 = new TCanvas("c4","nu11.4_K^{+}K^{-}E:y_vs_x"    );//,520,206,820,600);
   TCanvas *c5 = new TCanvas("c5","nu11.4_nn~E:y_vs_x"           );//,520,206,820,600);
   TCanvas *c6 = new TCanvas("c6","nu11.4_p^{+}p^{-}E:y_vs_x"    );//,520,206,820,600);
   TCanvas *c7 = new TCanvas("c7","nu11.4_othersE:y_vs_x"        );//,520,206,820,600);

   double a=40;
   double b=25;
   double xmin=-800;
   double xmax=+800;
   double ymin=-300;
   double ymax=+300;

   TH2F * xyplot1 = new TH2F("xyplot1","nu11.4_#gammaE:y_vs_x"        ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot2 = new TH2F("xyplot2","nu11.4_e^{+}e^{-}E:y_vs_x"    ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot3 = new TH2F("xyplot3","nu11.4_#pi^{+}#pi^{-}E:y_vs_x",a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot4 = new TH2F("xyplot4","nu11.4_K^{+}K^{-}E:y_vs_x"    ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot5 = new TH2F("xyplot5","nu11.4_nn~E:y_vs_x"           ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot6 = new TH2F("xyplot6","nu11.4_p^{+}p^{-}E:y_vs_x"    ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot7 = new TH2F("xyplot7","nu11.4_othersE:y_vs_x"        ,a,xmin,xmax,b,ymin,ymax);


   Long64_t nentries = chain->GetEntries();
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      chain->GetEntry(jentry);
      // if (Cut(ientry) < 0) continue;
		for (int nhits=0;nhits<n;nhits++) {
			if(pz[nhits]>0 && z[nhits]<12530. && z[nhits]>12505. && t[nhits]>40. && t[nhits]<50. )
			{
				if(abs(pid[nhits])==22)             xyplot1->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
				else if(abs(pid[nhits])==11)        xyplot2->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
				else if(abs(pid[nhits])==211)       xyplot3->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
				else if(abs(pid[nhits])==321)       xyplot4->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
				else if(abs(pid[nhits])==2112)      xyplot5->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
				else if(abs(pid[nhits])==2212)      xyplot6->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
				else                                xyplot7->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1.0/nentries);
			}
		}
   }

   double max=1e5;
   double min=1e-1;
   c1->cd();
   xyplot1->GetXaxis()->SetTitle("X(cm)" );
   xyplot1->GetYaxis()->SetTitle("Y(cm)" );
   xyplot1->GetYaxis()->SetTitleSize(0.045);
   xyplot1->GetYaxis()->SetTitleOffset(0.9);
   xyplot1->GetXaxis()->SetTitleSize(0.045);
   xyplot1->GetXaxis()->CenterTitle();
//   xyplot1->GetYaxis()->CenterTitle();
   xyplot1->SetMaximum(max);
   xyplot1->SetMinimum(min);
   xyplot1->Draw("colz");
   c1->SetLogz();

   c2->cd();
   xyplot2->GetXaxis()->SetTitle("X(cm)" );
   xyplot2->GetYaxis()->SetTitle("Y(cm)" );
   xyplot2->GetYaxis()->SetTitleSize(0.045);
   xyplot2->GetYaxis()->SetTitleOffset(0.9);
   xyplot2->GetXaxis()->SetTitleSize(0.045);
   xyplot2->GetXaxis()->CenterTitle();
//   xyplot2->GetYaxis()->CenterTitle();
   xyplot2->SetMaximum(max);
   xyplot2->SetMinimum(min);
   xyplot2->Draw("colz");
   c2->SetLogz();

   c3->cd();
   xyplot3->GetXaxis()->SetTitle("X(cm)" );
   xyplot3->GetYaxis()->SetTitle("Y(cm)" );
   xyplot3->GetYaxis()->SetTitleSize(0.045);
   xyplot3->GetYaxis()->SetTitleOffset(0.9);
   xyplot3->GetXaxis()->SetTitleSize(0.045);
   xyplot3->GetXaxis()->CenterTitle();
//   xyplot3->GetYaxis()->CenterTitle();
   xyplot3->SetMaximum(max);
   xyplot3->SetMinimum(min);
   xyplot3->Draw("colz");
   c3->SetLogz();

   c4->cd();
   xyplot4->GetXaxis()->SetTitle("X(cm)" );
   xyplot4->GetYaxis()->SetTitle("Y(cm)" );
   xyplot4->GetYaxis()->SetTitleSize(0.045);
   xyplot4->GetYaxis()->SetTitleOffset(0.9);
   xyplot4->GetXaxis()->SetTitleSize(0.045);
   xyplot4->GetXaxis()->CenterTitle();
//   xyplot4->GetYaxis()->CenterTitle();
   xyplot4->SetMaximum(max);
   xyplot4->SetMinimum(min);
   xyplot4->Draw("colz");
   c4->SetLogz();

   c5->cd();
   xyplot5->GetXaxis()->SetTitle("X(cm)" );
   xyplot5->GetYaxis()->SetTitle("Y(cm)" );
   xyplot5->GetYaxis()->SetTitleSize(0.045);
   xyplot5->GetYaxis()->SetTitleOffset(0.9);
   xyplot5->GetXaxis()->SetTitleSize(0.045);
   xyplot5->GetXaxis()->CenterTitle();
//   xyplot5->GetYaxis()->CenterTitle();
   xyplot5->SetMaximum(max);
   xyplot5->SetMinimum(min);
   xyplot5->Draw("colz");
   c5->SetLogz();

   c6->cd();
   xyplot6->GetXaxis()->SetTitle("X(cm)" );
   xyplot6->GetYaxis()->SetTitle("Y(cm)" );
   xyplot6->GetYaxis()->SetTitleSize(0.045);
   xyplot6->GetYaxis()->SetTitleOffset(0.9);
   xyplot6->GetXaxis()->SetTitleSize(0.045);
   xyplot6->GetXaxis()->CenterTitle();
//   xyplot6->GetYaxis()->CenterTitle();
   xyplot6->SetMaximum(max);
   xyplot6->SetMinimum(min);
   xyplot6->Draw("colz");
   c6->SetLogz();

   c7->cd();
   xyplot7->GetXaxis()->SetTitle("X(cm)" );
   xyplot7->GetYaxis()->SetTitle("Y(cm)" );
   xyplot7->GetYaxis()->SetTitleSize(0.045);
   xyplot7->GetYaxis()->SetTitleOffset(0.9);
   xyplot7->GetXaxis()->SetTitleSize(0.045);
   xyplot7->GetXaxis()->CenterTitle();
//   xyplot7->GetYaxis()->CenterTitle();
   xyplot7->SetMaximum(max);
   xyplot7->SetMinimum(min);
   xyplot7->Draw("colz");
   c7->SetLogz();

//   c1->Print("#gamma_xy.pdf");
//   c2->Print("e^{+}e^{-}_xy.pdf");
//   c3->Print("#pi^{+}#pi^{-}_xy.pdf");
//   c4->Print("K^{+}K^{-}_xy.pdf");
//   c5->Print("nn~_xy.pdf");
//   c6->Print("p^{+}p^{-}_xy.pdf");
//   c7->Print("others_xy.pdf");

   c1->Print("../plot_nu11.4/e/nu11.4_#gamma_xy.eps");
   c2->Print("../plot_nu11.4/e/nu11.4_e^{+}e^{-}_xy.eps");
   c3->Print("../plot_nu11.4/e/nu11.4_#pi^{+}#pi^{-}_xy.eps");
   c4->Print("../plot_nu11.4/e/nu11.4_K^{+}K^{-}_xy.eps");
   c5->Print("../plot_nu11.4/e/nu11.4_nn~_xy.eps");
   c6->Print("../plot_nu11.4/e/nu11.4_p^{+}p^{-}_xy.eps");
   c7->Print("../plot_nu11.4/e/nu11.4_others_xy.eps");

//   c1->Print("../plot_nu11.4/e/nu11.4_#gammaE_xy.gif");
//   c2->Print("../plot_nu11.4/e/nu11.4_e^{+}e^{-}E_xy.gif");
//   c3->Print("../plot_nu11.4/e/nu11.4_#pi^{+}#pi^{-}E_xy.gif");
//   c4->Print("../plot_nu11.4/e/nu11.4_K^{+}K^{-}E_xy.gif");
//   c5->Print("../plot_nu11.4/e/nu11.4_nn~E_xy.gif");
//   c6->Print("../plot_nu11.4/e/nu11.4_p^{+}p^{-}E_xy.gif");
//   c7->Print("../plot_nu11.4/e/nu11.4_othersE_xy.gif");


}
