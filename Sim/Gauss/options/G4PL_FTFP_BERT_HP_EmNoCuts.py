##
##  File containing options to activate the FTFP_BERT_HP Hadronic
##  Physics List in Geant4 to add low energy neutron processes

from Configurables import Gauss

Gauss().PhysicsList = {"Em":'NoCuts', "Hadron":'FTFP_BERT_HP', "GeneralPhys":True, "LHCbPhys":True}
