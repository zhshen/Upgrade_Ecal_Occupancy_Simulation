void b(){
   TTree *tree=0;  //!pointer to the analyzed TTree

   // Declaration of leaf types
   Int_t           n;
   Float_t         pid[20000];   //[n]
   Float_t         x[20000];   //[n]
   Float_t         y[20000];   //[n]
   Float_t         z[20000];   //[n]
   Float_t         t[20000];   //[n]
   Float_t         px[20000];   //[n]
   Float_t         py[20000];   //[n]
   Float_t         pz[20000];   //[n]
   Float_t         e[20000];   //[n]

   // List of branches
   TBranch        *b_n;   //!
   TBranch        *b_pid;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_t;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_e;   //!


   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("./mytuple_100ev_01.root");
   if (!f || !f->IsOpen()) {
      f = new TFile("./mytuple_100ev_01.root");
   }
   TDirectory * dir = (TDirectory*)f->Get("./mytuple_100ev_01.root:/GiGaGeo.PlaneDet.Tuple");
   dir->GetObject("Hits",tree);

   tree->SetBranchAddress("n", &n, &b_n);
   tree->SetBranchAddress("pid", pid, &b_pid);
   tree->SetBranchAddress("x", x, &b_x);
   tree->SetBranchAddress("y", y, &b_y);
   tree->SetBranchAddress("z", z, &b_z);
   tree->SetBranchAddress("t", t, &b_t);
   tree->SetBranchAddress("px", px, &b_px);
   tree->SetBranchAddress("py", py, &b_py);
   tree->SetBranchAddress("pz", pz, &b_pz);
   tree->SetBranchAddress("e", e, &b_e);


   gStyle->SetOptStat(0);

   TCanvas *c1 = new TCanvas("c1","#gammaE:y_vs_x"        );//,520,206,820,600);
   TCanvas *c2 = new TCanvas("c2","e^{+}e^{-}E:y_vs_x"    );//,520,206,820,600);
   TCanvas *c3 = new TCanvas("c3","#pi^{+}#pi^{-}E:y_vs_x");//,520,206,820,600);
   TCanvas *c4 = new TCanvas("c4","K^{+}K^{-}E:y_vs_x"    );//,520,206,820,600);
   TCanvas *c5 = new TCanvas("c5","nn~E:y_vs_x"           );//,520,206,820,600);
   TCanvas *c6 = new TCanvas("c6","p^{+}p^{-}E:y_vs_x"    );//,520,206,820,600); 
	TCanvas *c7 = new TCanvas("c7","othersE:y_vs_x"        );//,520,206,820,600); 
	double a=40;
   double b=30;
   double xmin=-800;
   double xmax=+800;
   double ymin=-300;
   double ymax=+300;

   TH2F * xyplot1 = new TH2F("xyplot1","#gammaE:y_vs_x"        ,a,xmin,xmax,b,ymin,ymax); 
	TH2F * xyplot2 = new TH2F("xyplot2","e^{+}e^{-}E:y_vs_x"    ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot3 = new TH2F("xyplot3","#pi^{+}#pi^{-}E:y_vs_x",a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot4 = new TH2F("xyplot4","K^{+}K^{-}E:y_vs_x"    ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot5 = new TH2F("xyplot5","nn~E:y_vs_x"           ,a,xmin,xmax,b,ymin,ymax); 
	TH2F * xyplot6 = new TH2F("xyplot6","p^{+}p^{-}E:y_vs_x"    ,a,xmin,xmax,b,ymin,ymax);
   TH2F * xyplot7 = new TH2F("xyplot7","othersE:y_vs_x"        ,a,xmin,xmax,b,ymin,ymax);


   Long64_t nentries = tree->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      nb = tree->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
		for (int nhits=0;nhits<n;nhits++) {
			if(pz[nhits]>0 && z[nhits]<12530. && z[nhits]>12505. && t[nhits]>40. && t[nhits]<50. ){
				if(abs(pid[nhits])==22)             xyplot1->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);
				else if(abs(pid[nhits])==11)        xyplot2->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);
				else if(abs(pid[nhits])==211)       xyplot3->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);
				else if(abs(pid[nhits])==321)       xyplot4->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);
				else if(abs(pid[nhits])==2112)      xyplot5->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);
				else if(abs(pid[nhits])==2212)      xyplot6->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);
				else                                xyplot7->Fill(x[nhits]/10.,y[nhits]/10.,e[nhits]*1./500.);				
			}
		}
   }

   double max=1e5;
   double min=1e-1;
   c1->cd();
   xyplot1->GetXaxis()->SetTitle("X(cm)" );
   xyplot1->GetYaxis()->SetTitle("Y(cm)" );
   xyplot1->GetYaxis()->SetTitleSize(0.045);
   xyplot1->GetYaxis()->SetTitleOffset(0.9);
   xyplot1->GetXaxis()->SetTitleSize(0.045);
   xyplot1->GetXaxis()->CenterTitle();
//   xyplot1->GetYaxis()->CenterTitle();
   xyplot1->SetMaximum(max);
   xyplot1->SetMinimum(min);
   xyplot1->Draw("colz");
   c1->SetLogz();

   c2->cd();
   xyplot2->GetXaxis()->SetTitle("X(cm)" );
   xyplot2->GetYaxis()->SetTitle("Y(cm)" );
   xyplot2->GetYaxis()->SetTitleSize(0.045);
   xyplot2->GetYaxis()->SetTitleOffset(0.9);
   xyplot2->GetXaxis()->SetTitleSize(0.045);
   xyplot2->GetXaxis()->CenterTitle();
//   xyplot2->GetYaxis()->CenterTitle();
   xyplot2->SetMaximum(max);
   xyplot2->SetMinimum(min);
   xyplot2->Draw("colz");
   c2->SetLogz();

   c3->cd();
   xyplot3->GetXaxis()->SetTitle("X(cm)" );
   xyplot3->GetYaxis()->SetTitle("Y(cm)" );
   xyplot3->GetYaxis()->SetTitleSize(0.045);
   xyplot3->GetYaxis()->SetTitleOffset(0.9);
   xyplot3->GetXaxis()->SetTitleSize(0.045);
   xyplot3->GetXaxis()->CenterTitle();
//   xyplot3->GetYaxis()->CenterTitle();
   xyplot3->SetMaximum(max);
   xyplot3->SetMinimum(min);
   xyplot3->Draw("colz");
   c3->SetLogz();

   c4->cd();
   xyplot4->GetXaxis()->SetTitle("X(cm)" );
   xyplot4->GetYaxis()->SetTitle("Y(cm)" );
   xyplot4->GetYaxis()->SetTitleSize(0.045);
   xyplot4->GetYaxis()->SetTitleOffset(0.9);
   xyplot4->GetXaxis()->SetTitleSize(0.045);
   xyplot4->GetXaxis()->CenterTitle();
//   xyplot4->GetYaxis()->CenterTitle();
   xyplot4->SetMaximum(max);
   xyplot4->SetMinimum(min);
   xyplot4->Draw("colz");
   c4->SetLogz();

   c5->cd();
   xyplot5->GetXaxis()->SetTitle("X(cm)" );
   xyplot5->GetYaxis()->SetTitle("Y(cm)" );
   xyplot5->GetYaxis()->SetTitleSize(0.045);
   xyplot5->GetYaxis()->SetTitleOffset(0.9);
   xyplot5->GetXaxis()->SetTitleSize(0.045);
   xyplot5->GetXaxis()->CenterTitle();
//   xyplot5->GetYaxis()->CenterTitle();
   xyplot5->SetMaximum(max);
   xyplot5->SetMinimum(min);
   xyplot5->Draw("colz");
   c5->SetLogz();

   c6->cd();
   xyplot6->GetXaxis()->SetTitle("X(cm)" );
   xyplot6->GetYaxis()->SetTitle("Y(cm)" );
   xyplot6->GetYaxis()->SetTitleSize(0.045);
   xyplot6->GetYaxis()->SetTitleOffset(0.9);
   xyplot6->GetXaxis()->SetTitleSize(0.045);
   xyplot6->GetXaxis()->CenterTitle();
//   xyplot6->GetYaxis()->CenterTitle();
   xyplot6->SetMaximum(max);
   xyplot6->SetMinimum(min);
   xyplot6->Draw("colz");
   c6->SetLogz();

   c7->cd();
   xyplot7->GetXaxis()->SetTitle("X(cm)" );
   xyplot7->GetYaxis()->SetTitle("Y(cm)" );
   xyplot7->GetYaxis()->SetTitleSize(0.045);
   xyplot7->GetYaxis()->SetTitleOffset(0.9);
   xyplot7->GetXaxis()->SetTitleSize(0.045);
   xyplot7->GetXaxis()->CenterTitle();
//   xyplot7->GetYaxis()->CenterTitle();
   xyplot7->SetMaximum(max);
   xyplot7->SetMinimum(min);
   xyplot7->Draw("colz");
   c7->SetLogz();

   c1->Print("#gamma_xyE.pdf");            
   c2->Print("e^{+}e^{-}_xyE.pdf");
   c3->Print("#pi^{+}#pi^{-}_xyE.pdf");
   c4->Print("K^{+}K^{-}_xyE.pdf");
   c5->Print("nn~_xyE.pdf");
   c6->Print("p^{+}p^{-}_xyE.pdf");
   c7->Print("others_xyE.pdf");

   c1->Print("#gamma_xyE.eps");            
   c2->Print("e^{+}e^{-}_xyE.eps");
   c3->Print("#pi^{+}#pi^{-}_xyE.eps");
   c4->Print("K^{+}K^{-}_xyE.eps");
   c5->Print("nn~_xyE.eps");
   c6->Print("p^{+}p^{-}_xyE.eps");
   c7->Print("others_xyE.eps");

   c1->Print("#gamma_xyE.gif");            
   c2->Print("e^{+}e^{-}_xyE.gif");
   c3->Print("#pi^{+}#pi^{-}_xyE.gif");
   c4->Print("K^{+}K^{-}_xyE.gif");
   c5->Print("nn~_xyE.gif");
   c6->Print("p^{+}p^{-}_xyE.gif");
   c7->Print("others_xyE.gif");              

	
}
