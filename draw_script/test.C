void test(){
   TTree *tree=0;  //!pointer to the analyzed TTree

   // Declaration of leaf types
   Int_t           n;
   Float_t         pid[20000];   //[n]
   Float_t         x[20000];   //[n]
   Float_t         y[20000];   //[n]
   Float_t         z[20000];   //[n]
   Float_t         t[20000];   //[n]
   Float_t         px[20000];   //[n]
   Float_t         py[20000];   //[n]
   Float_t         pz[20000];   //[n]
   Float_t         e[20000];   //[n]

   // List of branches
   TBranch        *b_n;   //!
   TBranch        *b_pid;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_z;   //!
   TBranch        *b_t;   //!
   TBranch        *b_px;   //!
   TBranch        *b_py;   //!
   TBranch        *b_pz;   //!
   TBranch        *b_e;   //!


   TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("./mytuple_100ev_01.root");
   if (!f || !f->IsOpen()) {
      f = new TFile("./mytuple_100ev_01.root");
   }
   TDirectory * dir = (TDirectory*)f->Get("./mytuple_100ev_01.root:/GiGaGeo.PlaneDet.Tuple");
   dir->GetObject("Hits",tree);

   tree->SetBranchAddress("n", &n, &b_n);
   tree->SetBranchAddress("pid", pid, &b_pid);
   tree->SetBranchAddress("x", x, &b_x);
   tree->SetBranchAddress("y", y, &b_y);
   tree->SetBranchAddress("z", z, &b_z);
   tree->SetBranchAddress("t", t, &b_t);
   tree->SetBranchAddress("px", px, &b_px);
   tree->SetBranchAddress("py", py, &b_py);
   tree->SetBranchAddress("pz", pz, &b_pz);
   tree->SetBranchAddress("e", e, &b_e);


   double count=0;


   Long64_t nentries = tree->GetEntriesFast();
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      nb = tree->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
		for (int nhits=0;nhits<n;nhits++) {
			if(pz[nhits]>0 && z[nhits]<12530. && z[nhits]>12505. )   //&& t[nhits]>40. && t[nhits]<50. )
			{
				count++;		
			}
		}
   }
   std::cout<<count<<std::endl;
}
